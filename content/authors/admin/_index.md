---
# Display name
name: Carlos E. Cuesta
avatar_image: "user-full.png"
# Username (this should match the folder name)
authors:
- admin
# resume download button
# btn:
# - url : "https://sourcethemes.com/academic/docs/install/"
#  label : "Download Resume"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Associate Professor

# Organizations/Affiliations
organizations:
- name: Rey Juan Carlos University
  url: "https://gestion2.urjc.es/pdi/ver/carlos.cuesta"

# Short bio (displayed in user profile at end of posts)
bio: My research interests are Software Architecture, Data Engineering and the many applications of both approaches to many fields in computing.

# Should the user's education and interests be displayed?
display_education: false

interests:
- Software Architecture
- Data Engineering

education:
  courses:
  - course: PhD in Information Technologies
    institution: University of Valladolid
    year: 2002
  - course: MEng in Computer Science
    institution: University of Valladolid
    year: 1997
  - course: BSc in Computer Science
    institution: University of Valladolid
    year: 1994

# Social/academia Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/cecuesta
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.es/citations?user=r_SrLZgAAAAJ&hl=en
- icon: github
  icon_pack: fab
  link: https://github.com/cecuesta
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups: []

---

Carlos E. Cuesta is an Associate Professor of Software Engineering, area of Computer Languages & Systems, at the School of Computer Science (ETSII) in King Juan Carlos Univerity (URJC). 

His research has mostly focused in Software Architecture, considered as the perspective from which to approach many other research areas in the field of  Computing, and particularly Data Engineering, Distributed Systems, Cybersecurity, Artificial Intelligence, Software Engineering and even Formal Methods.

![reviews](../../img/proc.png)

Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn